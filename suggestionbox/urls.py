from django.conf.urls import patterns, include, url
from suggestionbox import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^detail/(?P<suggestion_id>\d+)/$', views.detail, name='detail'),
)
